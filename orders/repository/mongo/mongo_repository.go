package mongo

import (
	"context"

	"github.com/jinzhu/gorm"
	"github.com/max-coffee/domain"
	"github.com/max-coffee/domain/repository"
)

type mongoOrdersRepo struct {
	DB *gorm.DB
}

func NewMongoOrdersRepository(db *gorm.DB) repository.OrdersRepository {
	return &mongoOrdersRepo{
		DB: db,
	}
}

func (self *mongoOrdersRepo) Orders(context.Context) (domain.Orders, error) {

	// TODO get payload

	var res domain.Orders

	return res, nil
}

func (self *mongoOrdersRepo) QueueOrders(context.Context) ([]string, error) {

	// TODO get queue orders

	res := []string{"a", "b", "c", "d", "e"}

	return res, nil
}

func (self *mongoOrdersRepo) UpdateQueueOrders(context.Context, domain.OrdersResult) error {

	// TODO update queue based on trxID

	return nil

}
