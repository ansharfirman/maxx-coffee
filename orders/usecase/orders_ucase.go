package usecase

import (
	"context"
	"time"

	"github.com/max-coffee/domain"
	"github.com/max-coffee/domain/repository"
)

type ordersUcase struct {
	ordersRepo repository.OrdersRepository
	ctxTimeout time.Duration
}

func NewOrdersUsecase(ordersRepo repository.OrdersRepository, timeout time.Duration) domain.OrdersUsecase {
	return &ordersUcase{
		ordersRepo: ordersRepo,
		ctxTimeout: timeout,
	}
}

func (self *ordersUcase) SendOrders(c context.Context) (domain.Orders, error) {

	ctx, cancel := context.WithTimeout(c, self.ctxTimeout)
	defer cancel()

	return self.ordersRepo.Orders(ctx)

}

func (self *ordersUcase) GetQueueOrders(c context.Context) ([]string, error) {

	ctx, cancel := context.WithTimeout(c, self.ctxTimeout)
	defer cancel()

	return self.ordersRepo.QueueOrders(ctx)

}

func (self *ordersUcase) UpdateQueueOrders(c context.Context, r domain.OrdersResult) error {

	ctx, cancel := context.WithTimeout(c, self.ctxTimeout)
	defer cancel()

	return self.ordersRepo.UpdateQueueOrders(ctx, r)

}
