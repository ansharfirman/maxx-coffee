package config

type Configuration struct {
	Server     server
	Context    context
	Database   database
	MaxxCoffee maxxCoffee
}

type context struct {
	Timeout int
}

type server struct {
	Address string
}

type database struct {
	Driver  string
	Host    string
	Port    string
	User    string
	Pass    string
	Name    string
	Ssl     string
	Timeout int
}

type maxxCoffee struct {
	Host string
}
