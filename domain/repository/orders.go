package repository

import (
	"context"

	"github.com/max-coffee/domain"
)

type OrdersRepository interface {
	Orders(context.Context) (domain.Orders, error)
	QueueOrders(context.Context) ([]string, error)
	UpdateQueueOrders(context.Context, domain.OrdersResult) error
}
