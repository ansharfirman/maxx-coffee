package domain

import "context"

type Orders struct {
}

type OrdersResult struct {
	TrxId  string
	Status string
}

type OrdersUsecase interface {
	SendOrders(ctx context.Context) (Orders, error)
	GetQueueOrders(ctx context.Context) ([]string, error)
	UpdateQueueOrders(ctx context.Context, res OrdersResult) error
}
