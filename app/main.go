package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/max-coffee/config"
	"github.com/max-coffee/orders/repository/mongo"
	"github.com/max-coffee/orders/usecase"
	"github.com/spf13/viper"
)

var Config config.Configuration

func init() {
	viper.SetConfigFile(`config.yaml`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {

	dbDriver := viper.GetString("database.driver")
	dbHost := viper.GetString("database.host")
	dbPort := viper.GetString("database.port")
	dbUser := viper.GetString("database.user")
	dbPass := viper.GetString("database.pass")
	dbName := viper.GetString("database.name")
	dbSslMode := viper.GetString("database.ssl")
	dbTimeout := viper.GetInt("database.timeout")

	if dbSslMode == "" {
		dbSslMode = "disable"
	}

	if dbTimeout <= 0 {
		dbTimeout = 5
	}

	dsn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s connect_timeout=%d", dbHost, dbPort, dbUser, dbName, dbPass, dbSslMode, dbTimeout)

	conn, err := ConnectDB(dbDriver, dsn)
	if err != nil {
		log.Fatal(err)
	}

	err = conn.DB().Ping()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err = conn.DB().Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	orders := mongo.NewMongoOrdersRepository(conn)
	timeoutCtx := time.Duration(Config.Context.Timeout) * time.Second
	ordersUcase := usecase.NewOrdersUsecase(orders, timeoutCtx)

	InitCron(ordersUcase, timeoutCtx)

	addr := viper.GetString("server.address")
	fmt.Println("Listening to port " + addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
