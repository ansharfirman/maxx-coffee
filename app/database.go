package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func ConnectDB(driver, dsn string) (*gorm.DB, error) {

	db, err := gorm.Open(driver, dsn)

	if err != nil {
		return nil, err
	}

	return db, nil
}
