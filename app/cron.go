package main

import (
	"context"
	"time"

	"github.com/max-coffee/domain"
	"github.com/robfig/cron"
)

var processing = false

func InitCron(ordersUcase domain.OrdersUsecase, timeout time.Duration) {
	c := context.Background()
	ctx, cancel := context.WithTimeout(c, time.Second*2) //change this second value, and see the different. Try with 7, 15 and see the different
	defer cancel()

	cr := cron.New()

	cr.AddFunc("@every 1s", func() {
		if !processing {
			SendOrders(&processing, ordersUcase, ctx)
		}
	})

	cr.Start()

}
