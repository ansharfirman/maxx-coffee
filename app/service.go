package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/max-coffee/domain"
	"github.com/spf13/viper"
)

func SendOrders(processing *bool, orderUcase domain.OrdersUsecase, ctx context.Context) {

	*processing = true

	queue, err := orderUcase.GetQueueOrders(ctx)
	if err != nil {
		log.Println(err.Error())
		return
	}

	var wg sync.WaitGroup
	resChannel := make(chan domain.OrdersResult)

	doSend := 0
	for _, row := range queue {
		wg.Add(1)
		go workerSendOrders(ctx, row, orderUcase, resChannel, &wg)
		doSend++
	}

	wg.Wait()

	for i := 0; i < doSend; i++ {
		res := <-resChannel
		log.Println(res)

		if res.Status == "ok" {
			orderUcase.UpdateQueueOrders(ctx, res)
		}

	}

	close(resChannel)

	return
}

func workerSendOrders(ctx context.Context, row string, orderUcase domain.OrdersUsecase, resChannel chan domain.OrdersResult, wg *sync.WaitGroup) {

	var resTrx domain.OrdersResult

	maxxCoffeURL := viper.GetString("maxxcoffee.host")

	handleReturn := func() {
		wg.Done()

		if r := recover(); r != nil {
			fmt.Println(" -- workerSendOrders Recovered from panic: ", r)
		}

		resChannel <- resTrx
	}

	defer handleReturn()

	payload, err := orderUcase.SendOrders(ctx)

	if err != nil {
		log.Println(err.Error())
		return
	}

	byteR, _ := json.Marshal(payload)
	netClient := &http.Client{
		Timeout: time.Second * 60,
	}
	resp, err := netClient.Post(maxxCoffeURL, "application/json", bytes.NewBuffer(byteR))

	if err != nil {
		log.Println(err.Error())
		return
	}

	if resp != nil {

		// todo handle response
		if resp.StatusCode == http.StatusOK {
			resTrx = domain.OrdersResult{
				TrxId:  row,
				Status: "ok",
			}
		}

		defer resp.Body.Close()
	}

}
